#ifndef __APPLICATION_HPP__
#define __APPLICATION_HPP__

#include "claw/application/Application.hpp"
#include "claw/application/DebugOverlay.hpp"
#include "claw/sound/mixer/effect/EffectVolumeShift.hpp"
#include "claw/sound/mixer/source/AudioPosition.hpp"
#include "claw/vfs/NativeMount.hpp"
#include "claw/base/Lunar.hpp"

#include "guif/Screen.hpp"

namespace Snake
{

    class SnakeApplication : public Claw::Application, Claw::LuaErrorListener
    {
    public:
        LUA_DEFINITION( SnakeApplication );
        SnakeApplication();
        ~SnakeApplication();

        void OnStartup() override;
        void OnShutdown() override;

        int l_GetBestResult( lua_State* L );
        int l_SetBestResult( lua_State* L );

        void OnUpdate( float dt ) override;
        void OnRender( Claw::Surface* target ) override;
        void OnKeyPress( Claw::KeyCode code ) override;
        void OnKeyRelease( Claw::KeyCode code ) override;
        void OnTouchDown( int x, int y, int button ) override;
        void OnTouchUp( int x, int y, int button ) override;
        void OnTouchMove( int x, int y, int button ) override;

        void OnLuaCallError( const Claw::LuaError* errorData ) override;

    private:
        const int MIXER_CHANNELS;
        const int MIXER_SAMPLING_FREQUENCY;
        const int MIXER_BUFFER_SIZE;
        const std::string RESULT_FILE_NAME;

        std::unique_ptr<Guif::Screen> m_guif;
    };

}

#endif
