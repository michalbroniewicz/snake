#include "Snake/Application.hpp"

namespace Snake
{

    LUA_DECLARATION( SnakeApplication )
    {
        METHOD( SnakeApplication, GetBestResult ),
        METHOD( SnakeApplication, SetBestResult ),
        {0,0}
    };

    SnakeApplication* application;

    SnakeApplication::SnakeApplication()
    : Claw::Application( Claw::AF_STENCIL )
    , MIXER_CHANNELS( 2 )
    , MIXER_SAMPLING_FREQUENCY( 44100 )
    , MIXER_BUFFER_SIZE( 4096 )
    , RESULT_FILE_NAME( "result.txt" )
    {
        m_guif = std::make_unique<Guif::Screen>();
        auto& lua = m_guif->Lua();
        lua.SetErrorListener( this );
    }

    SnakeApplication::~SnakeApplication()
    {
    }

    void SnakeApplication::OnLuaCallError( const Claw::LuaError* luaError )
    {
        CLAW_MSG( "Lua error, stack: " << luaError->stack );
    }

    static int QuitGame( lua_State* L )
    {
        application->Exit();
        return 0;
    }

    static int PlayClickSound( lua_State* L )
    {
        Claw::Mixer::Get()->Play( std::make_shared<Claw::AudioPosition>( Claw::AssetDict::Get<Claw::AudioSource>( "click.ogg@buffer" ) ), true );
        return 0;
    }

    int SnakeApplication::l_GetBestResult( lua_State* L )
    {
        Claw::VMStackGuard sg( L, 1 );

        auto lastResult = 0;

        if ( Claw::Vfs::Exists( RESULT_FILE_NAME ) )
        {
            std::string buffer;
            auto file = Claw::Vfs::Open( RESULT_FILE_NAME );
            file->ReadLine( buffer );
            file->Seek( 0, Claw::File::SM_SEEK_SET );
            lastResult = std::stoi( buffer );
        }

        lua_pushnumber( L, lastResult );
        return 1;
    }

    int SnakeApplication::l_SetBestResult( lua_State* L )
    {
        Claw::VMStackGuard sg( L );

        if ( lua_isnumber( L, 1 ) )
        {
            auto result = lua_tointeger( L, 1 );
            auto file = Claw::Vfs::Create( RESULT_FILE_NAME );
            file->WriteLine( std::to_string( result ) );
            file->Flush();
            file->Seek( 0, Claw::File::SM_SEEK_SET );
        }
        return 0;
    }

    void SnakeApplication::OnStartup()
    {
        CreateAssetDict();
        CreateTextDict();
        CreateRegistry();
        CreateMixer( Claw::AudioFormat( MIXER_CHANNELS, MIXER_SAMPLING_FREQUENCY ), Claw::MixerParams( MIXER_BUFFER_SIZE ) );
        Claw::DebugOverlay::Create();

#ifndef CLAW_SDL
        Claw::Vfs::Mount( "/", Claw::PakMount::Create( "data.pak" ) );
        Claw::Vfs::Mount( "/", std::make_shared<Claw::NativeMount>( "/save", Claw::MF_SAVEGAME | Claw::MF_EXTERNAL_STORAGE ) );
#else
        Claw::Vfs::Mount( "/", std::make_shared<Claw::NativeMount>( "../../res/" ) );
        Claw::Vfs::Mount( "/", std::make_shared<Claw::NativeMount>( "../../res/save", Claw::MF_SAVEGAME | Claw::MF_EXTERNAL_STORAGE ) );
#endif

        auto& lua = m_guif->Lua();

        Claw::Lunar<SnakeApplication>::Register( lua );
        Claw::Lunar<SnakeApplication>::push( lua, this );
        lua_setglobal( lua, "SnakeApplication" );

        m_guif->Lua().Load( "main.lua" );

        auto music_theme = Claw::Mixer::Get()->Register( Claw::AssetDict::Get<Claw::AudioSource>( "music.ogg" ) ).lock();
        music_theme->SetLoop( true );
        music_theme->AddEffect( std::make_shared<Claw::EffectVolumeShift>( Claw::Mixer::Get()->GetAudioFormat(), MIXER_CHANNELS ) );
        music_theme->SetPause( false );

        lua_pushcfunction( lua, QuitGame );
        lua_setglobal( lua, "QuitGame" );

        lua_pushcfunction( lua, PlayClickSound );
        lua_setglobal( lua, "PlayClickSound" );
    }

    void SnakeApplication::OnShutdown()
    {
    }

    void SnakeApplication::OnUpdate( float dt )
    {
        m_guif->Update( dt );
    }

    void SnakeApplication::OnRender( Claw::Surface* target )
    {
        target->Clear( 0 );
        m_guif->Render( target );
    }

    void SnakeApplication::OnKeyPress( Claw::KeyCode code )
    {
        m_guif->OnKeyDown( code );
    }

    void SnakeApplication::OnKeyRelease( Claw::KeyCode code )
    {
        m_guif->OnKeyUp( code );
    }

    void SnakeApplication::OnTouchDown( int x, int y, int button )
    {
        m_guif->OnTouchDown( x, y, button );
    }

    void SnakeApplication::OnTouchUp( int x, int y, int button )
    {
        m_guif->OnTouchUp( x, y, button );
    }

    void SnakeApplication::OnTouchMove( int x, int y, int button )
    {
        m_guif->OnTouchMove( x, y, button );
    }

}

CLAW_DEFINE_APPLICATION( Snake::application = new Snake::SnakeApplication, "Snake" );
