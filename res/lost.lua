local lost = {}

local config = load( "config.lua" )

local root = screen:GetControl( "/" )
local background = root:GetControl( "background" )
local lostRoot = background:GetControl( "lostRoot" )

local lostLogo = lostRoot:InsertControl( "youLost" )
local resultTextControl = lostRoot:InsertControl( "resultText" )
local backToMenuButton = lostRoot:InsertControl( "backToMenuButton" )

local screenSizeHolder = load( "screenSizeHolder.lua" )

function lost:CreateLostScreen()
    local buttonHeight = config:GetConst().buttonHeight
    local buttonWidth = config.GetConst().buttonWidth
    local controlVerticalDistance = config.GetConst().controlVerticalDistance

    self.resultTextWidth = 800

    local screenWidth = screenSizeHolder:GetWidth()
    local screenHeight = screenSizeHolder:GetHeight()

    local buttonPositionX = screenWidth / 2 - ( buttonWidth / 2 )
    local buttonPositionY = screenHeight / 2 - ( controlVerticalDistance / 2 ) - buttonHeight - ( buttonHeight + controlVerticalDistance )
    local resultTextPositionX = screenWidth / 2 - ( self.resultTextWidth / 2 )

    lostLogo:AddTree( "you_lost.xml" )
    lostLogo:SetPos( buttonPositionX, buttonPositionY )

    buttonPositionY = buttonPositionY + buttonHeight + controlVerticalDistance * 2

    resultTextControl:AddTree( "text.xml" )
    resultTextControl:SetPos( resultTextPositionX, buttonPositionY )

    buttonPositionY = buttonPositionY + buttonHeight + controlVerticalDistance * 2

    ConfigureDefaultButton( backToMenuButton, buttonHeight, buttonWidth, buttonPositionX, buttonPositionY, "Back to menu" )
end

function lost:UpdateResultText( actualResult )
    local lastResult = SnakeApplication:GetBestResult()

    if actualResult > lastResult then
        self.resultText = "Your score is " .. actualResult .. ". This is the new best result!"
        SnakeApplication:SetBestResult( actualResult )
    else
        self.resultText = "Your score is " .. actualResult .. ". Best result is " .. lastResult .. ""
    end
    resultTextControl:GetGraphic( "t" ):SetText( self.resultText )
end

return lost
