local config = {}

local Const = {
    EatableType = {
        FOOD = 1,
        TRAP = 2
    },
    ButtonType = {
        PLAY = 1,
        QUIT = 2,
        BACK_TO_MENU = 3
    },
    Direction = {
        LEFT = 1,
        RIGHT = 2,
        UP = 3,
        DOWN = 4
    },
    buttonHeight = 50,
    buttonWidth = 300,
    controlVerticalDistance = 30
}

function config:GetConst()
    return Const
end

return config
