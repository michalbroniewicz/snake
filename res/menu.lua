load( "util.lua" )

local menu = {}

local config = load( "config.lua" )

local root = screen:GetControl( "/" )
local background = root:GetControl( "background" )
local menuRoot = background:GetControl( "menuRoot" )

local logo = menuRoot:InsertControl( "logo" )
local playButton = menuRoot:InsertControl( "playButton" )
local quitButton = menuRoot:InsertControl( "quitButton" )

local screenSizeHolder = load( "screenSizeHolder.lua" )

function ConfigureDefaultButton( button, height, width, x, y, text )
    button:AddTree( "button.xml", nil, {
        left = ConstructFileName( "roundedbutton_smallpurple_left.png" ),
        center = ConstructFileName( "roundedbutton_smallpurple_center.png" ),
        right = ConstructFileName( "roundedbutton_smallpurple_left.png" ),
        flipr = true,
        text = text,
        lw = 20,
        rw = 20,
        ch = height,
        w = width
    } )
    button:SetPos( x, y )
    button:SetTouch( true )
end

function menu:CreateMenu()
    local buttonHeight = config:GetConst().buttonHeight
    local buttonWidth = config.GetConst().buttonWidth
    local controlVerticalDistance = config.GetConst().controlVerticalDistance

    local buttonPositionX = screenSizeHolder:GetWidth() / 2 - ( buttonWidth / 2 )
    local buttonPositionY = screenSizeHolder:GetHeight() / 2 - ( controlVerticalDistance / 2 ) - buttonHeight - ( buttonHeight + controlVerticalDistance )

    logo:AddTree( "logo.xml" )
    logo:SetPos( buttonPositionX, buttonPositionY )

    buttonPositionY = buttonPositionY + buttonHeight + controlVerticalDistance * 2

    ConfigureDefaultButton( playButton, buttonHeight, buttonWidth, buttonPositionX, buttonPositionY, "Play" )

    buttonPositionY = buttonPositionY + buttonHeight + controlVerticalDistance

    ConfigureDefaultButton( quitButton, buttonHeight, buttonWidth, buttonPositionX, buttonPositionY, "Quit" )
end

return menu
