local screenSizeHolder = {
    width = 0,
    height = 0
}

function screenSizeHolder:New( width, height )
    self.width = width
    self.height = height
end

function screenSizeHolder:GetWidth()
    return self.width
end

function screenSizeHolder:GetHeight()
    return self.height
end

screenSizeHolder:New( screen:GetSize() )

return screenSizeHolder
