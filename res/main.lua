local root = screen:GetControl( "/" )
local background = root:InsertControl( "background" )
background:AddTree( "background.xml" )

local menuRoot = background:InsertControl( "menuRoot" )
local gameRoot = background:InsertControl( "gameRoot" )
local lostRoot = background:InsertControl( "lostRoot" )

local config = load( "config.lua" )

gameRoot:SetVis( false )
lostRoot:SetVis( false )

local ButtonType = config:GetConst().ButtonType
local Direction = config:GetConst().Direction

local Buttons = {
    [ButtonType.PLAY] = "playButton",
    [ButtonType.QUIT] = "quitButton",
    [ButtonType.BACK_TO_MENU] = "backToMenuButton"
}

local menu = load( "menu.lua" )
local game = load( "game.lua" )
local lost = load( "lost.lua" )

menu:CreateMenu()
lost:CreateLostScreen()

function Screen:Update( dt )
    if game:IsRunning() then
        game:Update( dt )
    elseif game:IsLost() then
        local points = game:GetPoints()
        lost:UpdateResultText( points )
        gameRoot:SetVis( false )
        lostRoot:SetVis( true )
        game:ResetLost()
        game:Deinit()
    end
end

function Screen:OnTouchDown( x, y, id ) end

function Screen:OnTouchUp( x, y, id )
    PlayClickSound()
    local item = screen:GetTouchItem( x, y )
    if item then
        local itemName = item:GetName()
        if itemName == Buttons[ButtonType.PLAY] then
            menuRoot:SetVis( false )
            gameRoot:SetVis( true )
            game:CreateGameboard()
        elseif itemName == Buttons[ButtonType.QUIT] then
            QuitGame()
        elseif itemName == Buttons[ButtonType.BACK_TO_MENU] then
            lostRoot:SetVis( false )
            menuRoot:SetVis( true )
        end
    end
end

function Screen:OnTouchMove( x, y, id ) end

function Screen:OnKeyDown( code ) end

function Screen:OnKeyUp( code )
    if code == keys.KEY_LEFT then
        game:ChangeDirection( Direction.LEFT )
    elseif code == keys.KEY_RIGHT then
        game:ChangeDirection( Direction.RIGHT )
    elseif code == keys.KEY_UP then
        game:ChangeDirection( Direction.UP )
    elseif code == keys.KEY_DOWN then
        game:ChangeDirection( Direction.DOWN )
    end
end
