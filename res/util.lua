function ConstructFileName( img, extra )
    if extra then
        return img .. "@linear,mipmap-linear," .. extra
    else
        return img .. "@linear,mipmap-linear"
    end
end
