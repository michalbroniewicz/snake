local game = {}

local config = load( "config.lua" )

local EatableType = config:GetConst().EatableType
local Direction = config:GetConst().Direction

local root = screen:GetControl( "/" )
local background = root:GetControl( "background" )
local gameRoot = background:GetControl( "gameRoot" )

local screenSizeHolder = load( "screenSizeHolder.lua" )

function game:IsRunning()
    return self.isRunning
end

local function IsPositionAvailable( self, x, y )
    local foodX, foodY = self.food:GetPos()
    local trapX, trapY = self.trap:GetPos()

    if ( foodX == x and foodY == y )
    or ( trapX == x and trapY == y ) then
        return false
    end

    for _, segment in ipairs( self.snakeSegments ) do
        local segmentX, segmentY = segment:GetPos()
        if segmentX == x and segmentY == y then
            return false
        end
    end
    return true
end

local function InsertEatable( self, type )
    local drawn = false

    if #self.snakeSegments < ( self.gameboardX * self.gameboardY + 1 ) then
        while not drawn do
            local randomX = math.random( 0, self.gameboardX - 1 )
            local randomY = math.random( 0, self.gameboardY - 1 )

            if type == EatableType.FOOD then
                self.foodPosX = randomX * self.cellWidth
                self.foodPosY = randomY * self.cellHeight

                if IsPositionAvailable( self, self.foodPosX, self.foodPosY ) then
                    self.food:SetPos( self.foodPosX, self.foodPosY )
                    drawn = true
                end
            elseif type == EatableType.TRAP then
                local trapX = randomX * self.cellWidth
                local trapY = randomY * self.cellHeight

                if IsPositionAvailable( self, trapX, trapY ) then
                    self.trap:SetPos( trapX, trapY )
                    drawn = true
                end
            end
        end
    end
end

function game:CreateGameboard()
    self.snakeSegments = {}
    self.direction = Direction.RIGHT
    self.points = 0
    self.speed = 0.22
    self.acceleration = 0.75
    self.elapsedTime = 0
    self.maxSpeed = 0.07
    self.moved = true
    self.lost = false

    self.snakeHead = gameRoot:InsertControl( "snakeHead" )
    self.snakeHead:AddTree( "snake_segment.xml" )

    local _, _, cellW, cellH = self.snakeHead:GetSizeAbs()

    self.cellWidth, self.cellHeight = cellW, cellH

    local screenWidth = screenSizeHolder:GetWidth()
    local screenHeight = screenSizeHolder:GetHeight()

    self.gameboardX = math.floor( screenWidth / self.cellWidth )
    self.gameboardY = math.floor( screenHeight / self.cellHeight )

    local gameRootScaleX = screenWidth / ( self.gameboardX * self.cellWidth )
    local gameRootScaleY = screenHeight / ( self.gameboardY * self.cellHeight )

    gameRoot:SetMatrix( gameRootScaleX, 0, 0, gameRootScaleY )

    self.headX = math.floor( self.gameboardX / 2 ) * self.cellWidth
    self.headY = math.floor( self.gameboardY / 2 ) * self.cellHeight

    self.snakeHead:SetPos( self.headX, self.headY )

    table.insert( self.snakeSegments, self.snakeHead )

    self.nextX, self.nextY = self.snakeHead:GetPos()

    local secondSnakeSegment = gameRoot:InsertControl( "snakeSegment" .. #self.snakeSegments )
    secondSnakeSegment:AddTree( "snake_segment.xml" )
    secondSnakeSegment:SetPos( self.headX - self.cellWidth, self.headY )
    table.insert( self.snakeSegments, secondSnakeSegment )

    local thirdSnakeSegment = gameRoot:InsertControl( "snakeSegment" .. #self.snakeSegments )
    thirdSnakeSegment:AddTree( "snake_segment.xml" )
    thirdSnakeSegment:SetPos( self.headX - self.cellWidth * 2, self.headY )
    table.insert( self.snakeSegments, thirdSnakeSegment )

    self.food = gameRoot:InsertControl( "food" )
    self.food:AddTree( "food.xml" )

    self.trap = gameRoot:InsertControl( "trap" )
    self.trap:AddTree( "trap.xml" )

    InsertEatable( self, EatableType.FOOD )
    InsertEatable( self, EatableType.TRAP )

    self.isRunning = true
end

function game:Deinit()
    self.food:Remove()
    self.trap:Remove()

    for _, segment in ipairs( self.snakeSegments ) do
        segment:Remove()
    end
end

local function IsLosingMove( self )
    if self.nextX < 0
    or self.nextX > self.cellWidth * self.gameboardX - 1
    or self.nextY < 0
    or self.nextY > self.cellHeight * self.gameboardY - 1 then
        return true
    end

    local trapX, trapY = self.trap:GetPos()
    if self.nextX == trapX and self.nextY == trapY then
        return true
    end

    for _, segment in ipairs( self.snakeSegments ) do
        local x, y = segment:GetPos()
        if self.nextX == x and self.nextY == y then
            return true
        end
    end

    return false
end

local function LoseGame( self )
    self.lost = true
    self.isRunning = false
end

local function Move( self )
    if self.direction == Direction.RIGHT then
        self.nextX = self.nextX + self.cellWidth
    elseif self.direction == Direction.LEFT then
        self.nextX = self.nextX - self.cellWidth
    elseif self.direction == Direction.UP then
        self.nextY = self.nextY - self.cellHeight
    elseif self.direction == Direction.DOWN then
        self.nextY = self.nextY + self.cellHeight
    end

    if self.foodPosX == self.nextX and self.foodPosY == self.nextY then
        local newSegment = gameRoot:InsertControl( "snakeSegment" .. #self.snakeSegments )
        newSegment:AddTree( "snake_segment.xml" )
        table.insert( self.snakeSegments, newSegment )

        InsertEatable( self, EatableType.FOOD )
        InsertEatable( self, EatableType.TRAP )

        self.points = self.points + 1

        if self.speed > self.maxSpeed then
            self.speed = self.speed * self.acceleration
        end
    end

    if not IsLosingMove( self ) then
        for _, segment in ipairs( self.snakeSegments ) do
            local x, y = segment:GetPos()
            segment:SetPos( self.nextX, self.nextY )
            self.nextX, self.nextY = x, y
            self.moved = false
        end
    else
        LoseGame( self )
    end
end

function game:Update( dt )
    if self.elapsedTime >= self.speed then
        Move( self )
        self.elapsedTime = 0
    else
        self.elapsedTime = self.elapsedTime + dt
    end
    self.nextX, self.nextY = self.snakeHead:GetPos()
end

function game:ChangeDirection( direction )
    if not self.moved then
        if ( ( self.direction == Direction.UP
        or self.direction == Direction.DOWN )
        and ( direction == Direction.RIGHT
        or direction == Direction.LEFT ) )
        or ( ( self.direction == Direction.RIGHT
        or self.direction == Direction.LEFT )
        and ( direction == Direction.UP
        or direction == Direction.DOWN ) ) then
            self.direction = direction
            self.moved = true
        end
    end
end

function game:IsLost()
    return self.lost
end

function game:ResetLost()
    self.lost = false
end

function game:GetPoints()
    return self.points
end

return game
